package biz.wss.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.springframework.stereotype.Component;
import java.util.Date ;

/**
 * Returns the date as a string.
 *
 */
@Component
@Path("/date")
public class DateService 
{
	
	@GET
	@Path("/today")
	public String today() {
		return new Date().toString() ;
	}
	
}
